package com.stfalcon.glideimageviewer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.OnScaleChangedListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.stfalcon.glideimageviewer.adapter.RecyclingPagerAdapter;
import com.stfalcon.glideimageviewer.adapter.ViewHolder;

import java.util.HashSet;


/*
 * Created by troy379 on 07.12.16.
 */
class ImageViewerAdapter
        extends RecyclingPagerAdapter<ImageViewerAdapter.ImageViewHolder> {

    private Context context;
    private ImageViewer.DataSet<?> dataSet;
    private HashSet<ImageViewHolder> holders;

    private boolean isZoomingAllowed;

    ImageViewerAdapter(Context context, ImageViewer.DataSet<?> dataSet,
                       boolean isZoomingAllowed) {
        this.context = context;
        this.dataSet = dataSet;
        this.holders = new HashSet<>();

        this.isZoomingAllowed = isZoomingAllowed;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PhotoView drawee = new PhotoView(context);
        //drawee.setEnabled(isZoomingAllowed);


        ImageViewHolder holder = new ImageViewHolder(drawee);
        holders.add(holder);

        return holder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return dataSet.getData().size();
    }


    boolean isScaled(int index) {
        for (ImageViewHolder holder : holders) {
            if (holder.position == index) {
                return holder.isScaled;
            }
        }
        return false;
    }

    void resetScale(int index) {
        for (ImageViewHolder holder : holders) {
            if (holder.position == index) {
                  holder.resetScale();
                break;
            }
        }
    }



    String getUrl(int index) {
        return dataSet.format(index);
    }


    class ImageViewHolder extends ViewHolder implements OnScaleChangedListener {

        private int position = -1;
        private PhotoView drawee;
        private boolean isScaled;


        ImageViewHolder(View itemView) {
            super(itemView);
            drawee = (PhotoView) itemView;
            drawee.setOnScaleChangeListener(this);
        }

        void bind(int position) {
            this.position = position;
            Glide.with(context).load(dataSet.format(position))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))

                    .into(drawee);
        }

        @Override
        public void onScaleChange(float scaleFactor, float focusX, float focusY) {
            isScaled = drawee.getScale() > 1.1f;
        }


        //
        void resetScale() {
            drawee.setScale(1.0f, true);
        }


    }
}
